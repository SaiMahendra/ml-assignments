
# coding: utf-8

# In[71]:

import pandas as pd
import numpy as np
pop=pd.read_csv('state-population.csv',index_col='year')


# In[77]:

pop_s=pop.loc[pd.IndexSlice[2010]]
pop_2010_under18=pop_s[pop_s['ages']=='under18'].sort_values(['population'],ascending=False)
pop_2010_under18['populationrank'] = pop_2010_under18['population'].rank(ascending=0)
pop_2010_under18.head()


# In[78]:

pop_2010_total=pop_s[pop_s['ages']=='total'].sort_values(['population'],ascending=False)
pop_2010_total['populationrank'] = pop_2010_total['population'].rank(ascending=0)
pop_2010_total.head()

