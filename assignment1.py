In [1]: import numpy as np

In [2]: data=list([1,2,3,4,5,6])

In [3]: arr=np.array(data)

In [4]: arr1=arr*2

In [5]: arr1=arr*4

In [6]: arr2=arr+arr1

In [8]: arr
Out[8]: array([1, 2, 3, 4, 5, 6])

In [9]: arr1
Out[9]: array([ 4,  8, 12, 16, 20, 24])

In [10]: arr2
Out[10]: array([ 5, 10, 15, 20, 25, 30])

In [14]: %time arr = np.arange(10000000)
Wall time: 31.2 ms

In [15]: %time list1 =list(range(5000000))
Wall time: 125 ms


In [19]: import random


In [21]: [random.random() for _ in range(11)]
Out[21]:
[0.04347265341941864,
 0.7421630019722393,
 0.4690774202158675,
 0.78166653357309,
 0.703381354065867,
 0.8074986546689257,
 0.1009216379114024,
 0.015154034520788895,
 0.5170113462104182,
 0.9319141929752821,
 0.6171550456715861]

In [22]:[random.randint(0,100) for _ in range(20)]
Out[26]:[85, 50, 41, 99, 67, 63, 95, 97, 86, 2, 44, 21, 99, 90, 73, 21, 54, 2, 65,69]