
# coding: utf-8

# In[78]:


import pandas as pd
import numpy as np
ratings = pd.read_csv('ml-latest-small/ratings.csv')
ratings.head()


# In[86]:


movies = pd.read_csv('ml-latest-small/movies.csv')
movies.head()


# In[80]:


allratings = pd.merge(movies, ratings)
allratings['title'].value_counts()
lens=allratings.groupby('title').agg({'rating': [np.size, np.mean]})
size_100=lens['rating']['size'] >= 100
lens[size_100].sort_values([('rating', 'mean')], ascending=False)[:15]

