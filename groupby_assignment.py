# coding: utf-8

# In[599]:


import pandas as pd
import numpy as np
states = ['J & K', 'Punjab', 'Haryana', 'Uttarakhand',
          'Kerala', 'Tamil Nadu', 'Andhra Pradesh', 'Telangana']
group_key = ['North'] * 4 + ['South'] * 4
pd.DataFrame({'states':states,'key':group_key})
np.random.seed(5)
data = pd.Series(np.random.randn(8), index=states)
data[['Punjab', 'Kerala', 'Telangana']] = np.nan
df=pd.DataFrame({'key':group_key,'data':data})
df.isnull().any()
df.index.name='state'
df1=df.fillna(df.groupby('key').transform(np.mean))
df1.isnull().any()
np.equal(df1.groupby('key').mean(),df.groupby('key').mean(),dtype=float)

