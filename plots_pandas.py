
# coding: utf-8

# In[3]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')


# In[107]:


pop=pd.read_csv('state-population.csv')
ar=pd.read_csv('state-areas.csv',index_col='state')
abb=pd.read_csv('state-abbrevs.csv',index_col='state')
b1=(pd.concat([ar ,abb],axis=1))
b1.index.name='state'
b1.loc['Puerto Rico','abbreviation'] = 'PR'
b1=b1.reset_index()
s= pd.merge(pop, b1,left_on='state/region',
                  right_on='abbreviation', how='outer')
s = s.loc[(s['year'] == 2010), :]
s= s.loc[(s['ages'] == 'total'), :]
s.drop(['ages', 'year'], axis='columns', inplace=True)
s.loc[:, 'density'] = s['population'] / s['area (sq. mi)']
s=s.head(10)


# In[108]:


fig, ax = plt.subplots(2,figsize=(12, 20))
x = range(len(s.index))
ax[0].plot(x,s.density,"g",linestyle='--',label='Density')
ax[0].set_xticks(x)
ax[0].set_xticklabels(s.abbreviation)
ax[0].set_title("Density vs States of USA")
ax[0].set_xlabel('states',fontsize=20)
ax[0].set_ylabel('density',fontsize=20)
ax[0].legend(loc="upper right")
ax[0].annotate('maximum density', xy=(8, 8898), xytext=(4, 5000),
            arrowprops=dict(facecolor='blue', shrink=0.05),
              )
ax[0].grid(True)



ax[1].plot(x,s.population,"b",linestyle='-',label='Population')
ax[1].set_xticks(x)
ax[1].set_xticklabels(s.abbreviation)
ax[1].set_title("Population vs States of USA")
ax[1].legend(loc="upper left")
ax[1].set_xlabel('states',fontsize=20)
ax[1].set_ylabel('population',fontsize=20)
ax[1].annotate('maximum density', xy=(4, 37333601), xytext=(8, 25555000),
            arrowprops=dict(facecolor='red', shrink=0.05),
              )
ax[1].grid(True)

fig.savefig('statesofUSA.png')


