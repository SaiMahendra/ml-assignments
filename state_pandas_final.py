

import pandas as pd
import numpy as np
pop=pd.read_csv('state-population.csv')
ar=pd.read_csv('state-areas.csv',index_col='state')
abb=pd.read_csv('state-abbrevs.csv',index_col='state')
b1=(pd.concat([ar ,abb],axis=1))
b1.index.name='state'
b1.loc['Puerto Rico','abbreviation'] = 'PR'
b1=b1.reset_index()
s= pd.merge(pop, b1,left_on='state/region',
                  right_on='abbreviation', how='outer')
s = s.loc[(s['year'] == 2010), :]
s= s.loc[(s['ages'] == 'total'), :]
s.drop(['ages', 'year'], axis='columns', inplace=True)
s.loc[:, 'density'] = s['population'] / s['area (sq. mi)']
s.sort_values('density', ascending=False, inplace=True)
s.loc[:, ['state', 'density']]


