# coding: utf-8
import unittest
from distance import manhatt_dist
#x=[8,16,29,33,41,57,66,72,80,99]
#y=[9,17,28,34,40,58,65,73,81,98]
class TestDistance(unittest.TestCase):
    def test_manhatt_dist(self):
        x=[8,16,29,33,41,57,66,72,80,99]
        y=[9,17,28,34,40,58,65,73,81,98]
        res=manhatt_dist(x,y)
        self.assertEqual(res, 100)
        
    def test_manhatt_dist1(self):
        x=[8,16,29,33,41,57,66,72,80,99]
        y=[9,17,28,34,40,58,65,73,81,98]
        res=manhatt_dist(x,y)
        self.assertEqual(res, 10)
        
if __name__== '__main__':
    unittest.main()
    
