
# coding: utf-8

# In[1]:

import numpy as np
import pandas as pd
import sklearn.model_selection as ms
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.pipeline import FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Imputer
get_ipython().magic("run 'CategoricalEncoder.py'")
def read_file(x):
    df=pd.read_csv(x)
    return df
def categories(x,y,z):
    categories = x // y
    categories[categories >= z]= z
    return categories
def st_split(x,y):
    split = ms.StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
    for training_indices, test_indices in split.split(x,y):
        stratified_train_set = housing.loc[training_indices]
        stratified_test_set = housing.loc[test_indices]
    return stratified_train_set,stratified_test_set
def features_labels_numeric(x,y):
    features = stratified_train_set.drop(x, axis=1)
    labels = stratified_train_set[x].copy()
    numeric = features.drop(y, axis=1)
    return features,labels,numeric
def read_cols(x,y):
    numeric_cols = list(x)
    categorical_cols = [y]
    return numeric_cols,categorical_cols
class ColumnSelector(BaseEstimator, TransformerMixin):
    def __init__(self, column_names):
        self.column_names = column_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.column_names].values
rooms_col, bedrooms_col, population_col, households_col = 3, 4, 5, 6
class AttributeAdder(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self
    def transform(self, X, y=None):
        rph = X[:, rooms_col] / X[:, households_col]
        bph = X[:, bedrooms_col] / X[:, households_col]
        bpr = X[:, bedrooms_col] / X[:, rooms_col]
        pph = X[:, population_col] / X[:, households_col]
        return np.c_[X, rph, bph, bpr, pph]
def my_pipeline(x,y):
    numeric_cols_pipeline = Pipeline([('selector', ColumnSelector(x)),
                                  ('imputer', Imputer(strategy='median')),
                                  ('attributes_adder', AttributeAdder()),
                                  ('standard_scaler', StandardScaler())
                                 ])
    categorical_cols_pipeline = Pipeline([('selector', ColumnSelector(y)),
                                      ('encoder', CategoricalEncoder(encoding='onehot-dense'))
                                     ])
    full_pipeline = FeatureUnion(transformer_list=[('numeric_pipeline', numeric_cols_pipeline),
                                               ('categorical_pipeline', categorical_cols_pipeline)
                                              ])
    return full_pipeline


# In[2]:

housing=read_file('housing.csv')
income_categories=categories(housing['median_income'],1.5,5)
stratified_train_set,stratified_test_set=st_split(housing, income_categories)
housing_features,housing_labels,housing_numeric=features_labels_numeric('median_house_value','ocean_proximity')
numeric_cols,categorical_cols=read_cols(housing_numeric,'ocean_proximity')
housing_transformed = my_pipeline(numeric_cols,categorical_cols).fit_transform(housing)

