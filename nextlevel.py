def mymap(f):
    def mapped(lst):
        result = []
        for i in lst:
            result.append(f(i))
        return result
    return mapped
lst=[1,2,3,4,5,10]
inc=lambda x:x+1  #sqr=lambda x:x*x
mymap(inc)(lst)   #mymap(sqr)(lst)

#[2, 3, 4, 5, 6, 11]   #[1, 4, 9, 16, 25, 100]



def myfilter(pred):
    def filtered(lst):
        result = []
        for i in lst:
            if pred(i) == True:
                result.append(i)
        return result
    return filtered
lst=[1,2,3,4,5,7,10]
even=lambda x:x % 2 == 0 #odd=lambda x:x % 2 != 0
myfilter(even)(lst)      #myfilter(odd)(lst)

#[2, 4, 10]              #[1, 3, 5, 7]



def myreduce(bf,init):
    def reduced(lst):
        result = init
        for i in lst:
                result=bf(result,i)
        return result
    return reduced
lst=[1,2,3,4]
mysum=lambda x, y: x+y  #mul=lambda x, y: x*y
myreduce(mysum,0)(lst)  #myreduce(mul,0)(lst) 
#[10}                   #0


def myconcat(a, b):
    result = []
    for i in a:
        result.append(i)
    for i in b:
        result.append(i)
    return result
myconcat([1, 2, 3, 4],[5, 6, 7, 8, 9])
#[1, 2, 3, 4, 5, 6, 7, 8, 9]



def mydrop(first,lst):
    if first > 0:
        return lst[first-1 : ]
    elif first == 0:
        return lst
    else:
        return []
mydrop(5, [0,1,2,3,4,5,6,7,8,9,])
#[4, 5, 6, 7, 8, 9]



