def even(n):
    if (n % 2) == 0:return True
print(even(88))
#True



def odd(n):
    if (n % 2) != 0:return True
print(odd(101))
#True



def max (a,b):
    if a > b: return a
    else:return b
max(85,105)
#105



def maxof3 (a, b, c):return max(max(a,b), c)
maxof3(100,99,199)
#199



def factorial(n):
    result = 1
    for i in range(1,n + 1):result = result * i
    return result
factorial(7)
#5040



def mymax(first,*args):
    for e in args:
        if first < e:
            first=e
    return first
arr=[1,2,3,4,5,6,99,77]
mymax(*arr)
#99



def mysum(lst):
    result = 0
    for i in lst:
        result += i
    return result
mysum([1, 2, 3, 4, 5, 6])
#21



def myfactors(n):
    result = []
    for i in range(1, n):
        if (n % i) == 0:
            result.append(i)
    return result
myfactors(299)
#[1, 13, 23]



def myperfectnum(first,last):
    result = []
    for i in range(first,last + 1):
        if mysum(myfactors(i)) == i:
            result.append(i)
    return result
myperfectnum(1, 1999)
#[6, 28, 496]
