import json
from urllib.request import urlopen
url = "https://gdata.youtube.com/feeds/api/standardfeeds/top_rated?alt=json"
response = urlopen(url)
contents = response.read()
text = contents.decode('utf8')
data = json.loads(text)
for video in data['feed']['entry'][0:6]:
     print(video['title']['$t'])

"""---------------------------------------------------------------------------
JSONDecodeError                           Traceback (most recent call last)
<ipython-input-16-0fe8f431ff1b> in <module>()
      6 contents = response.read()
      7 text = contents.decode('utf8')
----> 8 data = json.loads(text)
      9 for video in data['feed']['entry'][0:6]:
     10      print(video['title']['$t'])

~\Anaconda3\lib\json\__init__.py in loads(s, encoding, cls, object_hook, parse_float, parse_int, parse_constant, object_pairs_hook, **kw)
    352             parse_int is None and parse_float is None and
    353             parse_constant is None and object_pairs_hook is None and not kw):
--> 354         return _default_decoder.decode(s)
    355     if cls is None:
    356         cls = JSONDecoder

~\Anaconda3\lib\json\decoder.py in decode(self, s, _w)
    337 
    338         
--> 339         obj, end = self.raw_decode(s, idx=_w(s, 0).end())
    340         end = _w(s, end).end()
    341         if end != len(s):

~\Anaconda3\lib\json\decoder.py in raw_decode(self, s, idx)
    355             obj, end = self.scan_once(s, idx)
    356         except StopIteration as err:
--> 357             raise JSONDecodeError("Expecting value", s, err.value) from None
    358         return obj, end

JSONDecodeError: Expecting value: line 1 column 1 (char 0)"""



#..........................................................................
import this

'''The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!'''




#..........................................................................
crypto_list = ['Yeti', 'Bigfoot', 'Loch Ness Monster']
crypto_string = '\n '.join(crypto_list)
print('Found and signing book deals:', crypto_string)


'''Found and signing book deals: Yeti
 Bigfoot
 Loch Ness Monster'''


#..........................................................................
    poem ='''I remember the night my mother
 was stung by a scorpion. Ten hours 
 of steady rain had driven him
 to crawl beneath a sack of rice.
 poured a little paraffin
 Upon the bitten toe and put a match to it'''

print(len(poem))
print(poem[:14])
print(poem.startswith('I'))
print(poem.endswith('That\'s all, folks!'))
word = 'scorpion'
print(poem.find(word))
print(poem.rfind(word))

'''201
I remember the
True
False
47
47'''


#..........................................................................
setup = 'i slapped with a large amount of space trout.......'
print(setup.strip('.'))
print(setup.capitalize())
print(setup.title())
print(setup.upper())
print(setup.lower())
print(setup.swapcase())
print(setup.center(30))
print(setup.ljust(30))
print(setup.rjust(30))
print(setup.replace('slapped', 'kicked'))
print(setup.replace('of ', 'of huge ', 100))


'''i slapped with a large amount of space trout
I slapped with a large amount of space trout.......
I Slapped With A Large Amount Of Space Trout.......
I SLAPPED WITH A LARGE AMOUNT OF SPACE TROUT.......
i slapped with a large amount of space trout.......
I SLAPPED WITH A LARGE AMOUNT OF SPACE TROUT.......
i slapped with a large amount of space trout.......
i slapped with a large amount of space trout.......
i slapped with a large amount of space trout.......
i kicked with a large amount of space trout.......
i slapped with a large amount of huge space trout.......'''

#.............................................................................
a_tuple = ('ready', 'fire', 'aim')
list(a_tuple)

'''['ready', 'fire', 'aim']'''

#.............................................................................
pythons = {
 'Chapman': 'Graham',
 'Cleese': 'John',
 'Idle': 'Eric',
 'Jones': 'Terry',
 'Palin': 'Michael',
 }
print(pythons)
pythons['Gilliam'] = 'Gerry'
print(pythons)
pythons['Gilliam'] = 'Terry'
print(pythons)
del pythons['Jones']
print(pythons)


'''{'Chapman': 'Graham', 'Cleese': 'John', 'Idle': 'Eric', 'Jones': 'Terry', 'Palin': 'Michael'}
{'Chapman': 'Graham', 'Cleese': 'John', 'Idle': 'Eric', 'Jones': 'Terry', 'Palin': 'Michael', 'Gilliam': 'Gerry'}
{'Chapman': 'Graham', 'Cleese': 'John', 'Idle': 'Eric', 'Jones': 'Terry', 'Palin': 'Michael', 'Gilliam': 'Terry'}'''
{'Chapman': 'Graham', 'Cleese': 'John', 'Idle': 'Eric', 'Palin': 'Michael', 'Gilliam': 'Terry'}


#.............................................................................
drinks = {
 'martini': {'vodka', 'vermouth'},
 'black russian': {'vodka', 'kahlua'},
 'white russian': {'cream', 'kahlua', 'vodka'},
 'manhattan': {'rye', 'vermouth', 'bitters'},
 'screwdriver': {'orange juice', 'vodka'}
}
for name, contents in drinks.items():
    if 'vodka' in contents:
        print(name)
print('')
        
for name, contents in drinks.items():
    if 'vodka' in contents and not ('vermouth' in contents or
    'cream' in contents):
        print(name)

        
'''martini
black russian
white russian
screwdriver

black russian
screwdriver'''

#.............................................................................
furry = True
small = True
if furry:
    if small:
        print("It's a cat.")
    else:
        print("It's a bear!")
else:
    if small:
        print("It's a skink!")
    else:
        print("It's a human. Or a hairless bear.")

        
'''It's a cat.'''     



#.............................................................................
color = "puce"
if color == "red":
    print("It's a tomato")
elif color == "green":
    print("It's a green pepper")
elif color == "bee purple":
    print("I don't know what it is, but only bees can see it")
else:
    print("I've never heard of the color", color)

'''I've never heard of the color puce'''


#.............................................................................
def commentary(color):
    if color == 'red':
        return "It's a tomato."
    elif color == "green":
        return "It's a green pepper."
    elif color == 'bee purple':
        return "I don't know what it is, but only bees can see it."
    else:
        return "I've never heard of the color " + color + "."
    
comment = commentary('blue')
print(comment)

'''I've never heard of the color blue.'''

#.............................................................................
def print_kwargs(**kwargs):
    print('Keyword arguments:', kwargs)
    
print_kwargs(wine='merlot', entree='mutton', dessert='macaroon')

'''Keyword arguments: {'wine': 'merlot', 'entree': 'mutton', 'dessert': 'macaroon'}'''


#.............................................................................
def outer(a, b):
    def inner(c, d):
         return c + d
    return inner(a, b)
outer(4,7)

'''11'''

#.............................................................................
def document_it(func):
     def new_function(*args, **kwargs):
        print('Running function:', func.__name__)
        print('Positional arguments:', args)
        print('Keyword arguments:', kwargs)
        result = func(*args, **kwargs)
        print('Result:', result)
        return result
     return new_function

def add_ints(a, b):
    return a + b

add_ints(3, 5)

cooler_add_ints = document_it(add_ints) 
cooler_add_ints(3, 5)

'''
Running function: add_ints
Positional arguments: (3, 5)
Keyword arguments: {}
Result: 8

8'''

#.............................................................................
def document_it(func):
     def new_function(*args, **kwargs):
        print('Running function:', func.__name__)
        print('Positional arguments:', args)
        print('Keyword arguments:', kwargs)
        result = func(*args, **kwargs)
        print('Result:', result)
        return result
     return new_function
def square_it(func):
     def new_function(*args, **kwargs):
        result = func(*args, **kwargs)
        return result * result
     return new_function
@document_it
@square_it
def add_ints(a, b):
    return a + b
add_ints(3, 5)

'''Running function: new_function
Positional arguments: (3, 5)
Keyword arguments: {}
Result: 64

64'''

#.............................................................................
short_list = [1, 2, 3]
position = 5
try:
    short_list[position]
except:
    print('Need a position between 0 and', len(short_list)-1, ' but got',
position)
    
'''Need a position between 0 and 2  but got 5'''

#.............................................................................
import sys
for place in sys.path:
    print(place)

'''C:\Users\Home\Anaconda3\python36.zip
C:\Users\Home\Anaconda3\DLLs
C:\Users\Home\Anaconda3\lib
C:\Users\Home\Anaconda3
C:\Users\Home\Anaconda3\lib\site-packages
C:\Users\Home\Anaconda3\lib\site-packages\Babel-2.5.0-py3.6.egg
C:\Users\Home\Anaconda3\lib\site-packages\win32
C:\Users\Home\Anaconda3\lib\site-packages\win32\lib
C:\Users\Home\Anaconda3\lib\site-packages\Pythonwin
C:\Users\Home\Anaconda3\lib\site-packages\IPython\extensions
C:\Users\Home\.ipython'''


#.............................................................................